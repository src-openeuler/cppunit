%global apiversion 1.15

Name:		cppunit
Version:	1.15.1
Release:	1
Summary:	C++ port of JUnit testing framework
License:	LGPLv2+
Url:		https://www.freedesktop.org/wiki/Software/cppunit/
Source:		http://dev-www.libreoffice.org/src/%{name}-%{version}.tar.gz

BuildRequires: doxygen gcc-c++ graphviz

%description
CppUnit is the C++ port of the famous JUnit framework for unit testing.\
Test output is in XML for automatic testing and GUI based for supervised tests.\

%package	devel
Summary:	Development files for %{name}
Requires:	%{name} = %{version}-%{release}

%description	devel
Libraries and header files of %{name} are all in the %{name}-devel package.

%package	help
Summary:	Help manual for %{name}
Provides:	%{name}-doc
Obsoletes:	%{name}-doc

%description	help
The %{name}-help package conatins man manual etc

%prep
%autosetup -n %{name}-%{version} -p1

%build
%configure --enable-doxygen --disable-static --disable-silent-rules
sed -i \
    -e 's|^hardcode_libdir_flag_spec=.*|hardcode_libdir_flag_spec=""|g' \
    -e 's|^runpath_var=LD_RUN_PATH|runpath_var=DIE_RPATH_DIE|g' \
    libtool
%make_build

%install
%make_install

rm %{buildroot}%{_libdir}/*.la
# remove double of doc
rm -rf %{buildroot}%{_datadir}/doc/cppunit %{buildroot}%{_datadir}/%{name}/html
# remove unused cppunit-config
rm -f %{buildroot}%{_bindir}/cppunit-config %{buildroot}%{_mandir}/man1/cppunit-config.1* %{buildroot}%{_datadir}/aclocal/cppunit.m4

# clean up examples
rm -rf __dist-examples __dist-examples-dir
cp -a examples __dist-examples
make -C __dist-examples distclean
# Makefile.am files are left as documentation
find __dist-examples \( -name Makefile.in -o -name .cvsignore -o -name '*.dsw' -o -name '*.dsp' \) -exec rm {} \;
mkdir __dist-examples-dir
mv __dist-examples __dist-examples-dir/examples


%ldconfig_scriptlets

%files
%license COPYING AUTHORS
%{_bindir}/DllPlugInTester
%{_libdir}/libcppunit-%{apiversion}.so.1
%{_libdir}/libcppunit-%{apiversion}.so.1.*

%files devel
%{_includedir}/cppunit
%{_libdir}/libcppunit.so
%{_libdir}/pkgconfig/cppunit.pc

%files help
%license COPYING
%doc NEWS README THANKS TODO BUGS doc/FAQ
%doc __dist-examples-dir/examples/
%doc doc/html

%changelog
* Tue Apr 19 2022 yaoxin <yaoxin30@h-partners.com> - 1.15.1-1
- Upgrade to version 1.15.1

* Mon Aug 02 2021 chenyanpanHW <chenyanpan@huawei.com> - 1.14.0-8
- DESC: delete -Sgit from %autosetup, and delete BuildRequires git

* Tue Dec 3 2019 openEuler Buildteam <buildteam@openeuler.org> - 1.14.0-7
- Package init
